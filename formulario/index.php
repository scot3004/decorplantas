<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width" charset="utf-8">
<title>Planeación Estratégica CBI</title>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>
<link href="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/css/style.css" rel="stylesheet" type="text/css" media="all" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" type="text/css" href="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/css/magnific-popup1.css">
<link rel="stylesheet" type="text/css" href="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/css/prettyPhoto.css">
<link href="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/css/layout.css" rel="stylesheet" type="text/css" />
        <link href="css/menu.css" rel="stylesheet" type="text/css" 
	<!--  jquery plguin -->
	<script type="text/javascript" src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/js/jquery.min.js"></script>
	<!--start slider -->
	    <link rel="stylesheet" href="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/css/fwslider.css" media="all">
	    <style type="text/css">
	    .client .title {
	font-family: Arial, Helvetica, sans-serif;
}
        </style>
	    <script src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/js/jquery-ui.min.js"></script>
		<script src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/js/css3-mediaqueries.js"></script>
		<script src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/js/fwslider.js"></script>
	<!--end slider -->
	 <script type="text/javascript">
			$(document).ready(function() {
			
				var defaults = {
		  			containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
		 		};
				
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
		<!-- Add fancyBox light-box -->
		<script src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/js/jquery.magnific-popup.js" type="text/javascript"></script>
				<script>
					$(document).ready(function() {
						$('.popup-with-zoom-anim').magnificPopup({
							type: 'inline',
							fixedContentPos: false,
							fixedBgPos: true,
							overflowY: 'auto',
							closeBtnInside: true,
							preloader: false,
							midClick: true,
							removalDelay: 300,
							mainClass: 'my-mfp-zoom-in'
					});
				});
		</script>
		<!-- //End fancyBox light-box -->

</head>
<body>
<form action="formulario.php" method="post" name="form1" id="form1">

<!-- start header -->
<div class="header_bg">
<div class="wrap">
	<div class="header">
	  <div class="logo">
	    <h1><a href="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/index.html"><img src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/images/logo.png" alt=""/></a></h1>
        
        <br/>
        <br/>
        
		</div>
		
		<div class="h_right">
			<ul class="menu">		
				<li class="active"><a  href="#home">INICIO</a>

		<ul class="submenu">	
		<li> <a href="#"> </a> </li> </ul>


				</li>


				<li><a href="#services" class="scroll">PLANEACIÓN 2014</a></li>
				<li><a href="#portfolio" class="scroll">FORMATOS</a></li>
				<li><a href="http://www.cbint.org/planeacionestrategica/conceptosgenerales/conceptosgenerales.html">CONCEPTOS</a></li>
				<li><a href="http://www.cbint.org/planeacionestrategica/lineamiento/lineamiento.html">LINEAMIENTOS</a></li>
				<li class="last"><a href="http://www.cbint.org/planeacionestrategica/criteriosinfgestion/criteriosinfgestion.html">CRITERIOS</a></li>
			<li><a  href="#home">CUENTA</a>

		<ul class="submenu">

		 <a href="#"> CUENTA1 </a> </li> </ul>

		 <ul class="submenu">

		<a href="#"> CUENTA2 </a> </li> </ul>

		<ul class="submenu">

		<a href="#"> CUENTA3 </a>  </li> </ul>

		<ul class="submenu">

		<a href="#"> CUENTA4 </a> </li> </ul>

		<ul class="submenu">

		<a href="#"> CUENTA5 </a> </li> </ul>





	</li>


			</ul>


	
    		</div>
			<script src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/js/classie.js"></script>
			<script src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/js/uisearch.js"></script>
			<script>
				new UISearch( document.getElementById( 'sb-search' ) );
			</script>
			<!-- start smart_nav * -->
	        <nav class="nav">
	            <ul class="nav-list">
	                <li class="nav-item"><a  href="#home">INICIO</a>

	                </li>
	                <li class="nav-item"><a href="#services" class="scroll">PLANEACIÓN 2014</a></li>
	                <li class="nav-item"><a href="#portfolio" class="scroll">FORMATOS</a></li>
	                <li class="nav-item"><a href="http://www.cbint.org/planeacionestrategica/conceptosgenerales/conceptosgenerales.html">CONCEPTOS</a></li>
	                <li class="nav-item"><a href="http://www.cbint.org/planeacionestrategica/lineamiento/lineamiento.html">LINEAMIENTOS</a></li>
	                <li class="nav-item"><a href="http://www.cbint.org/planeacionestrategica/criteriosinfgestion/criteriosinfgestion.html">CRITERIOS</a></li>
	                <div class="clear"></div>
	            </ul>
	        </nav>
	        <script type="text/javascript" src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/js/responsive.menu.js"></script>
			<!-- end smart_nav * -->
		</div>
		<div class="clear"></div>
	</div>
</div>
</div>



<!----start-images-slider---->
		<div class="images-slider">
			<!-- start slider -->
		    <div id="fwslider">
		        <div class="slider_container">
		            <div class="slide"> 
		                <!-- Slide image -->
		                    <img src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/images/slider-bg.jpg" alt=""/>
		                <!-- /Slide image -->
		                <!-- Texts container -->
		                <div class="slide_content">
		                    <div class="slide_content_wrap">
		                        <!-- Text title -->
		                        <h4 class="title">PLANEACIÓN ESTRATÉGICA</h4>
		                        <!-- /Text title -->
		                        <!-- Text description -->
		                        <p class="description">...</p>
		                        <!-- /Text description -->
		                        <div class="slide-btns description">	                      
		                        </div>
		                    </div>
		                </div>
		                 <!-- /Texts container -->
		            </div>
		            <!-- /Duplicate to create more slides -->
		            <div class="slide">
		                <img src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/images/slider-bg2.jpg" alt=""/>
		                <div class="slide_content">
		                     <div class="slide_content_wrap">
		                        <!-- Text title -->
		                        <h4 class="title">Planeación estratégica</h4>
		                        <!-- /Text title -->
		                        <!-- Text description -->
		                        <p class="description"></p>
		                        <!-- /Text description -->
		                        <div class="slide-btns description">		                      
		                        </div>
		                    </div>
		                </div>
		            </div>
                    <div class="slide">
		                <img src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/images/slider-bg3.jpg" alt=""/>
		                <div class="slide_content">
		                     <div class="slide_content_wrap">
		                        <!-- Text title -->
		                        <h4 class="title">PLANEACIÓN ESTRATÉGICA</h4>
		                        <!-- /Text title -->
		                        <!-- Text description -->
		                        <p class="description"></p>
		                        <!-- /Text description -->
		                        <div class="slide-btns description">		                      
		                        </div>
		                    </div>
		                </div>
		            </div>
		            <!--/slide -->
		        </div>
		        <div class="timers"> </div>
		        <div class="slidePrev"><span> </span></div>
		        <div class="slideNext"><span> </span></div>
		    </div>
		    <!--/slider -->
		</div>
<!-----------service------------>
<div  class="sevice" id="services">
	<div class="wrap">
		<div class="service-grids">
			<h2>PLANEACIÓN ESTRATÉGICA 2014</h2>
						<div class="images_1_of_4">
					 		<a href="http://www.cbint.org/planeacionestrategica/misionvision/misionvision.html"><img src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/images/cal.png"> </a>
					 		<h3><a href="http://www.cbint.org/planeacionestrategica/misionvision/misionvision.html">Misión - Visión </a></h3>
					 		<p>La visión es una declaración que indica hacia dónde se dirige la iglesia en el largo plazo, o qué es aquello en lo que pretende convertirse. <br/>(ver más...)</p>				    
						</div>
						<div class="images_1_of_4">
							 <a href="http://www.cbint.org/planeacionestrategica/principiosyvalores/principiosyvalores.html"><img src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/images/port.png"></a>
							 <h3><a href="http://www.cbint.org/planeacionestrategica/principiosyvalores/principiosyvalores.html">Principios - Valores</a></h3>
							 <p>Son cualidades positivas que posee la iglesia, tales como la búsqueda de la excelencia, el desarrollo de la comunidad, <br/>(ver más...)</p>						   
						</div>
						<div class="images_1_of_4">
							 <a href="http://www.cbint.org/planeacionestrategica/objetivosgenerales/objetivosgenerales.html"><img src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/images/recyle.png"></a>
							 <h3><a href="http://www.cbint.org/planeacionestrategica/objetivosgenerales/objetivosgenerales.html">Objetivos generales</a></h3>
							 <p>Permitan lograr la misión y capitalizar las oportunidades externas y fortalezas internas, superar las amenazas externas y debilidades internas. <br/>(ver más...) </p>						   
						</div>
						<div class="images_1_of_4">
							 <a href="http://www.cbint.org/planeacionestrategica/Objetivos/general/todosobjetivos.html"><img src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/images/camera.png"></a>
							 <h3><a href="http://www.cbint.org/planeacionestrategica/Objetivos/general/todosobjetivos.html">Objetivos específicos</a></h3>
							 <p>Estos objetivos se establecen teniendo en cuenta los recursos o la capacidad de la iglesia, así como la situación del entorno. <br/>(ver más...)</p>						   
						</div>
						<div class="clear"> </div>
		 </div>
</div>
</div>
<!-----------//service//------------>
<!----------- message ------------>
<div class="message">
	<div class="wrap">
		
   
    </div>
    <div class="clear"></div>	
</div>	
</div>
</div>	
  
  <div id="cbp-so-scroller" class="cbp-so-scroller">
        <!--Portfolio-->
        <div id="portfolio" class="clearfix">
            <div class="portfolio-bg"> </div>
            <div class="typo1">
                <div class="wrapper clearfix">
                    <div class="section-title">
                       <h3 class="heading">FORMATOS PARA EL DESARROLLO - PLANEACIÓN ESTRATÉGICA</h3>					  
					   <h6> </h6>
                    </div><!--end:section-title-->
                    
                    <div id="options" class="clearfix">
                        <ul id="filters" class="option-set clearfix" data-option-key="filter">
                        	 <li><a href="#filter" data-option-value=".objetivo">Objetivos</a></li>
                            <li><a href="#filter" data-option-value=".planeacion">Planeación Estratégica</a></li>
                            <li><a href="#filter" data-option-value=".estrategias">Estrategias</a></li>
                            <li><a href="#filter" data-option-value=".cronograma">Cronograma</a></li>
                             <li><a href="#filter" data-option-value=".brief">Brief</a></li>
                            <li><a href="#filter" data-option-value=".presupuesto">Presupuesto</a></li>
                            <li><a href="#filter" data-option-value=".registro">Registro de actividades</a></li>


                            
                                                    
                        </ul><!--end:filters-->
                    </div>
                          
                </div><!--end:wrapper-->
                
                <ul id="container" class="clickable cs-style-5 grid clearfix isotope" style="position: relative; overflow: hidden; height: 534px; width: 1475px;">
            
                    <li class="element objetivo isotope-item" style="position: absolute; left: 0px; top: 0px; -webkit-transform: translate3d(0px, 0px, 0px);">
                      <figure>
                          <a href="http://www.cbint.org/planeacionestrategica/Objetivos/general/todosobjetivos.html"><img src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/images/pf-1.jpg" alt=""></a>
                          <figcaption>
                                <h3><a href="http://www.cbint.org/planeacionestrategica/Objetivos/general/todosobjetivos.html">Objetivos</a></h3>
                                <div class="meta-box clearfix">
                                    <span class="entry-categories"><a href="http://www.cbint.org/planeacionestrategica/Objetivos/general/todosobjetivos.html">específicos</a></span>
                                </div>
                                <footer>
                                   <div id="nivo-lightbox-demo"><p> <a href="http://www.cbint.org/planeacionestrategica/Objetivos/general/todosobjetivos.html" data-lightbox-gallery="gallery1" id="nivo-lightbox-demo">Ver</a> </p></div>                                   
                                </footer>
                            </figcaption>
                        </figure>
                       
                    </li>
                        
                    <li class="element planeacion isotope-item" style="position: absolute; left: 0px; top: 0px; color:95C11E; -webkit-transform: translate3d(295px, 0px, 0px);">
                      <figure>
                          <a href="http://www.cbint.org/planeacionestrategica/planeacionestrategica.html"><img src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/images/pf-2.jpg" alt=""></a>
                          <figcaption>
                                <h3><a href="http://www.cbint.org/planeacionestrategica/planeacionestrategica.html">Planeación</a></h3>
                                <div class="meta-box clearfix">
                                    <span class="entry-categories"><a href="http://www.cbint.org/planeacionestrategica/planeacionestrategica.html">estratégica</a></span>
                                </div>
                                <footer>
                                    <div id="nivo-lightbox-demo"> <p> <a href="http://www.cbint.org/planeacionestrategica/planeacionestrategica.html" data-lightbox-gallery="gallery1" id="nivo-lightbox-demo"> Ver</a> </p></div>                                  
                                </footer>
                            </figcaption>
                        </figure>
                        
                    </li>
                        
                    <li class="element estrategias isotope-item" style="position: absolute; left: 0px; top: 0px; -webkit-transform: translate3d(590px, 0px, 0px);">
                      <figure>
                          <a href="http://www.cbint.org/planeacionestrategica/estrategias/estrategia.php"><img src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/images/pf-3.jpg" alt=""></a>
                          <figcaption>
                                <h3><a href="http://www.cbint.org/planeacionestrategica/estrategias/estrategia.php">Estrategias</a></h3>
                                <div class="meta-box clearfix">
                                </div>
                                <footer>
                                    <div id="nivo-lightbox-demo"> <p> <a href="http://www.cbint.org/planeacionestrategica/estrategias/estrategia.php" data-lightbox-gallery="gallery1" id="nivo-lightbox-demo">Ver</a> </p></div>                           
                                </footer>
                            </figcaption>
                        </figure>
                        
                    </li>
                        
                    <li class="element cronograma isotope-item" style="position: absolute; left: 0px; top: 0px; -webkit-transform: translate3d(885px, 0px, 0px);">
                      <figure>
                          <a href="http://www.cbint.org/planeacionestrategica/cronogramadeactividades/cronograma.html"><img src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/images/pf-4.jpg" alt=""></a>
                          <figcaption>
                                <h3><a href="http://www.cbint.org/planeacionestrategica/cronogramadeactividades/cronograma.html">Cronograma de actividades</a></h3>
                                <div class="meta-box clearfix">
                                    
                                </div>
                                <footer>
                                    <div id="nivo-lightbox-demo"> <p> <a href="http://www.cbint.org/planeacionestrategica/cronogramadeactividades/cronograma.html" data-lightbox-gallery="gallery1" id="nivo-lightbox-demo">Ver</a> </p></div>                                  
                                </footer>
                            </figcaption>
                        </figure>
                        
                    </li>
                        
                    <li class="element brief isotope-item" style="position: absolute; left: 0px; top: 0px; -webkit-transform: translate3d(1180px, 0px, 0px);">
                      <figure>
                          <a href="http://www.cbint.org/planeacionestrategica/brief/Brief.html"><img src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/images/pf-5.jpg" alt=""></a>
                          <figcaption>
                                <h3><a href="http://www.cbint.org/planeacionestrategica/brief/Brief.html">Brief</a></h3>
                                <div class="meta-box clearfix">
                                    <span class="entry-categories"><a href="http://www.cbint.org/planeacionestrategica/brief/Brief.html">Medios y comunicaciones</a></span>
                                   
                                </div>
                                <footer>
                                     <div id="nivo-lightbox-demo"> <p> <a href="http://www.cbint.org/planeacionestrategica/brief/Brief.html" data-lightbox-gallery="gallery1" id="nivo-lightbox-demo">Ver</a> </p></div>                                   
                                </footer>
                            </figcaption>
                        </figure>
                       
                    </li>
                        
                    <li class="element presupuesto isotope-item" style="position: absolute; left: 0px; top: 0px; -webkit-transform: translate3d(0px, 267px, 0px);">
                      <figure>
                          <a href="http://www.cbint.org/planeacionestrategica/presupuesto/presupuesto.html"><img src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/images/pf-6.jpg" alt=""></a>
                          <figcaption>
                                <h3><a href="http://www.cbint.org/planeacionestrategica/presupuesto/presupuesto.html">Presupuesto</a></h3>
                                <div class="meta-box clearfix">
                                </div>
                                <footer>
                                     <div id="nivo-lightbox-demo"> <p> <a href="http://www.cbint.org/planeacionestrategica/presupuesto/presupuesto.html" data-lightbox-gallery="gallery1" id="nivo-lightbox-demo">Ver</a> </p></div>                                    
                                </footer>
                            </figcaption>
                        </figure>
                        
                    </li>
                        
                    <li class="element registro isotope-item" style="position: absolute; left: 1px; top: 1px; -webkit-transform: translate3d(295px, 267px, 0px);">
                      <figure>
                          <a href="http://www.cbint.org/planeacionestrategica/registrodeactividades/registrodeactividades.html"><img src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/images/pf-7.jpg" alt=""></a>
                          <figcaption>
                                <h3><a href="http://www.cbint.org/planeacionestrategica/registrodeactividades/registrodeactividades.html">Registro de Actividades</a></h3>
                                <div class="meta-box clearfix">
                                </div>
                                <footer>
                                     <div id="nivo-lightbox-demo"> <p> <a href="http://www.cbint.org/planeacionestrategica/registrodeactividades/registrodeactividades.html" data-lightbox-gallery="gallery1" id="nivo-lightbox-demo">Ver</a> </p></div>                                    
                                </footer>
                        </figcaption>
                      </figure>
                                
                                
                                <!-- #container -->

           <!--end:typo1-->
             
    </div>
    <!-- portfolio_script_javascript-->
     <script type="text/javascript" src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/js/jquery.min.js"></script>
     <script src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/js/jquery.isotope.min.js"></script> 
   	 <script src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/js/jquery.prettyPhoto.js"></script>     
	 <script src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/js/custom.js"></script>  
	 <script src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/js/modernizr.custom.js"></script>
<!----------- message1 ------------>
<div class="message1">
<div class="wrap">
		<h3><div style="font-size:66px">...</div></h3>
		<p><div style="font-size:40px">...</div></p>
</div>
</div>






<!-----------start-pricing----------->																					
							<!-----pop-up-grid---->
								 <div id="small-dialog" class="mfp-hide">
									<div class="pop_up">
									 	<div class="payment-online-form-left">
<form>
												<h4><span class="shipping"> </span>Shipping</h4>
												<ul>
													<li><input class="text-box-dark" type="text" value="Frist Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Frist Name';}"></li>
													<li><input class="text-box-dark" type="text" value="Last Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Last Name';}"></li>
												</ul>
												<ul>
													<li><input class="text-box-dark" type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}"></li>
													<li><input class="text-box-dark" type="text" value="Company Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Company Name';}"></li>
												</ul>
												<ul>
													<li><input class="text-box-dark" type="text" value="Phone" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Phone';}"></li>
													<li><input class="text-box-dark" type="text" value="Address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Address';}"></li>
													<div class="clear"> </div>
												</ul>
												<div class="clear"> </div>
											<ul class="payment-type">
												<h4><span class="payment"> </span> Payments</h4>
												<li><span class="col_checkbox">
													<input id="3" class="css-checkbox1" type="checkbox">
													<label for="3" name="demo_lbl_1" class="css-label1"> </label>
													<a class="visa" href="#"> </a>
													</span>
													
												</li>
												<li>
													<span class="col_checkbox">
														<input id="4" class="css-checkbox2" type="checkbox">
														<label for="4" name="demo_lbl_2" class="css-label2"> </label>
														<a class="paypal" href="#"> </a>
													</span>
												</li>
												<div class="clear"> </div>
											</ul>
												<ul>
													<li><input class="text-box-dark" type="text" value="Card Number" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Card Number';}"></li>
													<li><input class="text-box-dark" type="text" value="Name on card" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name on card';}"></li>
													<div class="clear"> </div>
												</ul>
												<ul>
													<li><input class="text-box-light hasDatepicker" type="text" id="datepicker" value="Expiration Date" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Expiration Date';}"><em class="pay-date"> </em></li>
													<li><input class="text-box-dark" type="text" value="Security Code" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Security Code';}"></li>
													<div class="clear"> </div>
												</ul>
												<ul class="payment-sendbtns">
													<li><input type="reset" value="Cancel"></li>
													<li><input type="submit" value="Process order"></li>
												</ul>
												<div class="clear"> </div>
											</form>
										</div>
						  			</div>
								</div>
								<!-----pop-up-grid---->
						  
                        
                          
                       
<!-- Add fancyBox light-box -->
				<!-- Add fancyBox light-box -->
		<script src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/js/jquery.magnific-popup.js" type="text/javascript"></script>
				<script>
					$(document).ready(function() {
						$('.popup-with-zoom-anim').magnificPopup({
							type: 'inline',
							fixedContentPos: false,
							fixedBgPos: true,
							overflowY: 'auto',
							closeBtnInside: true,
							preloader: false,
							midClick: true,
							removalDelay: 300,
							mainClass: 'my-mfp-zoom-in'
					});
				});
		</script>
		<!-- //End fancyBox light-box -->
		<!----End-pricingplans---->
<!-----------end-pricing------------->
<!----------- message2 ------------>
<div class="message3"></div>
<!--------start-contact-----------> 
 <div class="contact" id="contact">
	<div class="wrap">
		
	  <div class="contact-form">
				<div class="form">
					<h3>COMENTARIOS</h3>
<form action="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/formulario.php" method="post" name="form1" id="form1">


<div>
  <input name="nombre" type="text" onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Nombre';}" value="Nombre">
								    </div>



<div>
  <input name="email" type="text" onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = ' E-mail';}" value="E-mail">
								    </div>
  

  
	 <div>   	<textarea name="Mensaje" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = ' Mensaje';}" value="Mensaje">Mensaje</textarea>
	      </div>
								<div class="span4">
							    	<a href="#">
							    		<i><input type="submit" value="Enviar"></i>
							    		<div class="clear"></div>
							    	</a>
							    </div>								
				  </form>
		</div>
				<div class="para-contact">
					<h4>INFORMES</h4>				
		 		  <div class="get-intouch-left-address">
		 		  	    <p>Angela Lara, Ext. 1235</p>
						<p>Procesos - CBI Internacional</p>
						
					<p>procesos@cbint.org</p>
				  </div>
					<div class="span4">
							    	<a href="#">
							 
						    		<div class="clear"></div>
							    	</a>
			      </div>		
					<div class="clear"> </div>	
				</div>
						<a class="mov-top" href="#home1"> <span> </span></a>
					 <div class="clear"> </div>
	  </div>
   </div>
</div>
<!--------//end-contact-----------><!--- footer-top --->
<div class="footer-bottom">
	<div class="wrap">
		<div class="image" align="center">
			<a href="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/index.html"><img src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/images/logo.png"></a>
		</div>	
		
		 <div class="clear"></div>
	</div>
</div>		
 <!-- scroll_top_btn -->
		<script type="text/javascript" src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/js/move-top.js"></script>
		<script type="text/javascript" src="../../../../../../private/var/folders/pt/9y1t_7bs31g3qjn6sypz5_780000gp/T/fz3temp-1/js/easing.js"></script>
		<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
			});
		});
	</script>

		 <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
</body>
</html>